epicsEnvSet("AFG_IP",    "$(AFG3000_IP=10.4.3.200)")   # Choose afg ethernet address
epicsEnvSet("AFG_PREFIX", "$(AFG3000_PREFIX=AFG3102C)") # Choose site prefix name
epicsEnvSet("AFG_ASYN_PORT",  "AFG3102C")  # Choose asyn port name
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", 100000)

# Load software support
require "afg3000"

# Setup IOC->hardware link
vxi11Configure("$(AFG_ASYN_PORT)", "$(AFG_IP)", 0, 0.0, "inst0", 0)

# Load records
dbLoadRecords("AFG3102C.db","PREFIX=$(AFG_PREFIX),ASYN_PORT=$(AFG_ASYN_PORT),ASYN_ADDR=0")

# Enable asyn trace
asynSetTraceMask($(AFG_ASYN_PORT),0, 0x9)
asynSetTraceIOMask($(AFG_ASYN_PORT),0, 0x2)
